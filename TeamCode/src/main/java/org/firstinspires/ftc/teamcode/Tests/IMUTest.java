package org.firstinspires.ftc.teamcode.Tests;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.teamcode.Hardware.MecanumDriveTrain;
import org.firstinspires.ftc.teamcode.Hardware.VisionTracker;

@Disabled
@TeleOp(name = "IMU/Vision Test", group = "Test")
public class IMUTest extends OpMode {

    MecanumDriveTrain drive = null;
    VisionTracker vision = null;

    @Override
    public void init() {

        drive = new MecanumDriveTrain();
        vision = new VisionTracker();

        drive.init(hardwareMap);
        vision.init(hardwareMap);

    }

    @Override
    public void init_loop() {


    }

    @Override
    public void start() {

        vision.activateTensor();


    }

    @Override
    public void loop() {

        telemetry.addData("Heading", drive.imu.getHeading());
        telemetry.addData("gold x/y", vision.goldCoordBoth()[0] + "/" + vision.goldCoordBoth()[1]);


    }

    @Override
    public void stop() {


    }

}
