package org.firstinspires.ftc.teamcode.Hardware;

public enum Coordinates {

  RED_DEPOT(+60, -60),
  BLUE_DEPOT(-60, +60),
  RED_CRATER(-60, -60),
  BLUE_CRATER(+60, +60),
  RED_RIGHT_START(+24, -24),
  RED_LEFT_START(-24, -24),
  BLUE_RIGHT_START(-24, +24),
  BLUE_LEFT_START(+24, +24),
  MOON(0, -72),
  CRATER(-72, 0),
  ROVER(0, +72),
  STARS(+72, 0);


  private double x;
  private double y;

  public double[] coords() {
    return new double[]{x, y};
  }

  private Coordinates(double x, double y) {
    this.x = x;
    this.y = y;
  }

  public static double[] craterCoords(boolean isRed) {
    if (isRed)
      return RED_CRATER.coords();
    else
      return BLUE_CRATER.coords();
  }

  public static double[] depotCoords(boolean isRed) {
    if (isRed)
      return RED_DEPOT.coords();
    else
      return BLUE_DEPOT.coords();
  }

  public static double[] startCoords(boolean isRed, boolean isRight) {
    if (isRed && isRight)
      return RED_RIGHT_START.coords();
    else if (isRed)
      return RED_LEFT_START.coords();
    else if (isRight)
      return BLUE_RIGHT_START.coords();
    else
      return BLUE_LEFT_START.coords();
  }

  public static double[] nearMark(boolean isRed) {
    if (isRed)
      return MOON.coords();
    else
      return ROVER.coords();
  }

}

