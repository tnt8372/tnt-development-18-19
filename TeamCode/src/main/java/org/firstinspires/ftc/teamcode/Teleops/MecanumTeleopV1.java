package org.firstinspires.ftc.teamcode.Teleops;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.util.Range;

@Disabled
@TeleOp(name = "Mecanum Drive (test)", group = "Teleop")
public class MecanumTeleopV1 extends OpMode {

  DcMotor motorTR = null;
  DcMotor motorTL = null;
  DcMotor motorBR = null;
  DcMotor motorBL = null;

  double prop = 0;

  @Override
  public void init() {

    motorTR = hardwareMap.dcMotor.get("motorTR");
    motorTL = hardwareMap.dcMotor.get("motorTL");
    motorBR = hardwareMap.dcMotor.get("motorBR");
    motorBL = hardwareMap.dcMotor.get("motorBL");

  }

  @Override
  public void init_loop() {

  }

  @Override
  public void start() {

  }

  @Override
  public void loop() {

    if(gamepad1.right_bumper)
      prop = 0.5;
    else if(gamepad1.left_bumper)
      prop = 0.25;
    else
      prop = 1;

    if(gamepad1.a)
      motorTR.setPower(1);
    if(gamepad1.b)
      motorTL.setPower(1);
    if(gamepad1.x)
      motorBR.setPower(1);
    if(gamepad1.y)
      motorBL.setPower(1);

    motorTR.setPower(prop * Range.clip(+gamepad1.left_stick_x + gamepad1.left_stick_y + gamepad1.right_stick_x, -1, 1)); //motors
    motorTL.setPower(prop * Range.clip(+gamepad1.left_stick_x - gamepad1.left_stick_y + gamepad1.right_stick_x, -1, 1));
    motorBR.setPower(prop * Range.clip(-gamepad1.left_stick_x + gamepad1.left_stick_y + gamepad1.right_stick_x, -1, 1));
    motorBL.setPower(prop * Range.clip(-gamepad1.left_stick_x - gamepad1.left_stick_y + gamepad1.right_stick_x, -1, 1));

  }

  @Override
  public void stop() {



  }

}
