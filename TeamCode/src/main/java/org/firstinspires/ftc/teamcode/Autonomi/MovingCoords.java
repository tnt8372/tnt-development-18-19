package org.firstinspires.ftc.teamcode.Autonomi;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;

import org.firstinspires.ftc.teamcode.Hardware.MecanumDriveTrain;
import org.firstinspires.ftc.teamcode.Hardware.VisionTracker;

@Disabled
@Autonomous(name = "Movement Test", group = "Test")
public class MovingCoords extends OpMode {

  MecanumDriveTrain drive;
  VisionTracker vuforia;

  @Override
  public void init() {

    drive = new MecanumDriveTrain();
    drive.init(hardwareMap);

    vuforia = new VisionTracker();
    vuforia.init(hardwareMap);

  }

  @Override
  public void init_loop() {



  }

  @Override
  public void start() {

    vuforia.activateVuforia();

  }

  @Override
  public void loop() {



    vuforia.endLoop();
    drive.imu.endLoop();

  }

  @Override
  public void stop() {

    vuforia.deactivateVuforia();

  }

}
