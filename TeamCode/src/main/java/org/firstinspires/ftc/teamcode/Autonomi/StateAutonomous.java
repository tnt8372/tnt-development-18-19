package org.firstinspires.ftc.teamcode.Autonomi;

import com.qualcomm.hardware.rev.RevTouchSensor;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.Hardware.Coordinates;
import org.firstinspires.ftc.teamcode.Hardware.MecanumDriveTrain;
import org.firstinspires.ftc.teamcode.Hardware.Sequence;
import org.firstinspires.ftc.teamcode.Hardware.State;
import org.firstinspires.ftc.teamcode.Hardware.Stopwatch;
import org.firstinspires.ftc.teamcode.Hardware.VisionTracker;

import static org.firstinspires.ftc.teamcode.Hardware.ServoPositions.*;


@Autonomous(name = "StateAutoV1", group = "")
public class StateAutonomous extends OpMode {

    MecanumDriveTrain drive = null;

    public final static double DEPLOY_UP = 0.5;
    public final static double DEPLOY_DOWN = 0;

    Stopwatch stopwatch = null;
    Stopwatch totalStopwatch = null;

    double depotIn = 0;
    double depotOut = 0;
    double goldOff = 0;
    double parked = 0;

    VisionTracker vision = null;

    Sequence sequence = null;

    DcMotor collectorExtension = null;
    DcMotor depositorExtension = null;
    DcMotor lift = null;
    DcMotor collectorLift = null;

    DigitalChannel liftLimit = null;
    DigitalChannel depositorLimit = null;
    DigitalChannel collectorLimit = null;

    Servo depositorGate = null;
    Servo collectorGate = null;
    CRServo collector = null;
    Servo depositorAngle = null;

    double temp = 0;

    boolean startRed = true;
    boolean startRight = false;
    boolean startUp = true;

    boolean prevUp = false;
    boolean prevDown = false;

    @Override
    public void init() {

        stopwatch = new Stopwatch();
        totalStopwatch = new Stopwatch();

        drive = new MecanumDriveTrain();
        drive.init(hardwareMap);

        liftLimit = hardwareMap.get(DigitalChannel.class, "liftLimit");
        depositorLimit = hardwareMap.get(DigitalChannel.class, "depositorLimit");
        collectorLimit = hardwareMap.get(DigitalChannel.class, "collectorLimit");

        collectorExtension = hardwareMap.dcMotor.get("collectorExtension");
        depositorExtension = hardwareMap.dcMotor.get("depositorExtension");
        lift = hardwareMap.dcMotor.get("lift");
        collectorLift = hardwareMap.dcMotor.get("collectorLift");

        depositorGate = hardwareMap.servo.get("depositorGate");
        collector = hardwareMap.crservo.get("collector");
        collectorGate = hardwareMap.servo.get("collectorGate");
        depositorAngle = hardwareMap.servo.get("depositorAngle");

        collectorLift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        collectorExtension.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        depositorExtension.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        lift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        vision = new VisionTracker();
        vision.init(hardwareMap);

        sequence = new Sequence();

    }

    @Override
    public void init_loop() {

        if (gamepad1.a)
            startRight = true;
        else if (gamepad1.b)
            startRed = true;
        else if (gamepad1.y)
            startRight = false;
        else if (gamepad1.x)
            startRed = false;
        else if (gamepad1.right_bumper)
            startUp = true;
        else if (gamepad1.left_bumper)
            startUp = false;

        if (gamepad1.dpad_up && !prevUp)
            sequence.nextDelay += 1;
        else if (gamepad1.dpad_down && !prevDown && sequence.nextDelay > 0)
            sequence.nextDelay -= 1;

        prevUp = gamepad1.dpad_up;
        prevDown = gamepad1.dpad_down;

        telemetry.addData("Starting", startRed ? "Red" : "Blue");
        telemetry.addData("Starting", startRight ? "Right" : "Left");
        telemetry.addData("Starting", startUp ? "pulled up" : "on the ground");
        telemetry.addData("Delay", sequence.nextDelay);

        lift.setPower(0);
        collectorLift.setPower(0);
        collectorExtension.setPower(0);
        depositorExtension.setPower(0);

        depositorGate.setPosition(DEPOSIT_CLOSED);
        collectorGate.setPosition(COLLECTOR_CLOSED + 0.14);
        collector.setPower(0);

    }

    @Override
    public void start() {

//        if (startRed && startRight)   // red and right
//            drive.setOffset(45);
//        else if (startRed)            // red and left
//            drive.setOffset(135);
        if (startRight)          // blue and right
            drive.setOffset(-135);
        else                         // blue and left
            drive.setOffset(-45);

        sequence.add(State.DELAY);
        sequence.add(State.MOVE_FROM_LANDER);
        sequence.add(State.KNOCKING_GOLD_SAMPLE);
        if (!startRight)
            sequence.add(State.LATCH_PICTURE);
        else
            sequence.add(State.MOVE_THRU_SAMPLE);
        sequence.add(State.DELAY);
        sequence.add(State.STOP);

        vision.activateTensor();

        stopwatch.reset();
        totalStopwatch.reset();
        totalStopwatch.unpause();
    }

    @Override
    public void loop() {

        switch (sequence.current()) {
            case DELAY:
                drive.stop();
                stopwatch.unpause();
                if (stopwatch.get() >= sequence.nextDelay) {
                    sequence.nextState();
                    stopwatch.reset();
                }
                break;

            case MOVE_FROM_LANDER:
                switch (sequence.getPhase()) {
                    case 0: //for letting down latch
                        lift.setPower(1);
                        if (!liftLimit.getState() || !startUp) {
                            lift.setPower(0);
                            sequence.nextPhase();
                            if(!startRight)
                                sequence.setPhase(4);
                        }
                        break;

                    case 1:
                        lift.setPower(0);
                        drive.turnTo(drive.imu.denormalizeAngle(-drive.imu.headingOffset + 90), 1);
                        stopwatch.unpause();
                        if (drive.atTargetHeading() || stopwatch.get() > 2) {
                            drive.stop();
                            sequence.nextPhase();
                            drive.startEncoders();
                            stopwatch.reset();
                        }
                        break;

                    case 2:

                        if(collectorExtension.getCurrentPosition()> 800) {
                            lowerCollector();
                            if(depotIn == 0)
                                depotIn = totalStopwatch.get();
                        }

                        if (collectorExtension.getCurrentPosition() > 1600){
                            collectorExtension.setPower(0);
                        } else {
                            collectorExtension.setPower(0.8);
                        }

                        if(drive.atEncoderValue(400))
                            drive.stop();
                        else
                            drive.moveAtAngle(0.225, 0);

                        if(drive.atEncoderValue(400) && collectorExtension.getCurrentPosition() > 1600) {
                            sequence.nextPhase();
                            drive.startEncoders();
                        }


                        break;

                    case 3:

                        collector.setPower(0.85);

                        if(drive.atEncoderValue(375))
                            drive.stop();
                        else
                            drive.moveAtAngle(0.225, 180);

                        if (collectorExtension.getCurrentPosition() < 100){
                            collectorExtension.setPower(0);
                        } else {
                            collectorExtension.setPower(-0.8);
                        }

                        if(collectorExtension.getCurrentPosition() < 1200) {
                            if(depotOut == 0)
                                depotOut = totalStopwatch.get();
                            raiseCollector();
                        }
                        else
                            lowerCollector();

                        if (collectorExtension.getCurrentPosition() < 100 && drive.atEncoderValue(375)) {
                            sequence.nextPhase();
                            collectorExtension.setPower(0);
                            collectorLift.setPower(0);
                            stopwatch.reset();
                        }
                        break;

                    case 4: //for slipping out of hook
                        collector.setPower(0);
                        lift.setPower(0);

//            drive.turnTo(Range.clip(drive.imu.denormalizeAngle(drive.imu.getHeading() + 45), drive.imu.denormalizeAngle(-drive.imu.headingOffset), drive.imu.denormalizeAngle(-drive.imu.headingOffset + 160)));
                        drive.turnTo(drive.imu.denormalizeAngle(-drive.imu.headingOffset + 155), 1);
                        stopwatch.unpause();
                        if (drive.atTargetHeading() || stopwatch.get() > 2) {
                            drive.stop();
                            sequence.nextState();
                            drive.startEncoders();
                            stopwatch.reset();
                        }
                        break;

                }
                break;

            case KNOCKING_GOLD_SAMPLE:
                switch (sequence.getPhase()) {
                    case 0:
                        stopwatch.unpause();
                        if (stopwatch.get() > 1.25) {
                            sequence.nextPhase();
                            stopwatch.reset();
                        } else if (vision.goldCoord() != -1) {
                            vision.lastGoldPlacement = -1;
                            sequence.nextPhase();
                            sequence.nextPhase();
                            stopwatch.reset();
                            vision.deactivateTensor();
                            vision.activateVuforia();
                        }
                        telemetry.addData("gold coord", vision.goldCoord());
                        break;

                    case 1:
                        drive.turnTo(-drive.imu.headingOffset + 185, 0.4);
                        if (drive.atTargetHeading())
                            stopwatch.unpause();
                        if (stopwatch.get() > 1.25) {
                            vision.lastGoldPlacement = 1;
                            sequence.nextPhase();
                            stopwatch.reset();
                            vision.deactivateTensor();
                            vision.activateVuforia();
                        } else if (vision.goldCoord() != -1) {
                            vision.lastGoldPlacement = 0;
                            sequence.nextPhase();
                            stopwatch.reset();
                            vision.deactivateTensor();
                            vision.activateVuforia();
                        }
                        telemetry.addData("gold coord", vision.goldCoord());
                        break;

                    case 2:
                        drive.turnTo(-drive.imu.headingOffset + vision.angleOnGold(startRight, startRed), 0.3);
                        if (drive.atTargetHeading()) {
                            sequence.nextState();
                            drive.startEncoders();
                            drive.stop();
                        }
                        break;

                }
                break;

            case MOVE_THRU_SAMPLE:
                switch (sequence.getPhase()) {
                    case 0:
                        collectorExtension.setPower(0.5);
                        collector.setPower(-0.85);
                        lowerCollector(1);
                        if (collectorExtension.getCurrentPosition() > 1300)
                            sequence.nextPhase();
                        break;

                    case 1:
                        collectorExtension.setPower(-0.8);
                        raiseCollector();
                        if (!collectorLimit.getState() || collectorExtension.getCurrentPosition() < 100) {
                            sequence.nextPhase();
                            collectorExtension.setPower(0);
                            collectorLift.setPower(0);
                            goldOff = totalStopwatch.get();
                        }
                        break;

                    case 2:
                        storeCollector();
                        collector.setPower(0);
                        stopwatch.reset();
                        drive.turnTo(drive.imu.headingOffset - 155);
                        if (drive.atTargetHeading() || stopwatch.get() > 3.5) {
                            sequence.nextPhase();
                            stopwatch.reset();
                        }
                        break;

                    case 3:
                        collectorLift.setPower(0);
                        drive.moveAtAngle(0.55, -90);
                        stopwatch.unpause();
                        if (stopwatch.get() > 2) {
                            stopwatch.reset();
                            sequence.nextPhase();
                        }
                        break;

                    case 4:
                        drive.turnTo(0);
                        stopwatch.unpause();
                        if (drive.atTargetHeading() || stopwatch.get() > 2) {
                            drive.stop();
                            sequence.nextPhase();
                            stopwatch.reset();
                            drive.startEncoders();
                        }
                        break;

                    case 5:
                        stopwatch.unpause();
                        drive.moveAtAngle(0.5, 180);
                        if (drive.atEncoderValue(800) || stopwatch.get() > 2) {
                            drive.stop();
                            stopwatch.reset();
                            sequence.nextState();
                            parked = totalStopwatch.get();
                        }
                        break;
                }
                break;

            case LATCH_PICTURE:
                switch (sequence.getPhase()) {
                    case 0:
                        stopwatch.unpause();
                        if(drive.atEncoderValue(100))
                            drive.stop();
                        else
                            drive.moveAtAngle(0.15, 0);
                        collectorExtension.setPower(0.5);
                        lowerCollector(1);
                        if (collectorExtension.getCurrentPosition() > (vision.lastGoldPlacement == 1 ? 1000 : 1500) || stopwatch.get() > 3)
                            sequence.nextPhase();
                        break;

                    case 1:
                        collectorExtension.setPower(-0.8);
                        raiseCollector();
                        if (!collectorLimit.getState() || collectorExtension.getCurrentPosition() < 100) {
                            sequence.nextPhase();
                            collectorExtension.setPower(0);
                            collectorLift.setPower(0);
                            goldOff = totalStopwatch.get();
                        }
                        break;

                    case 2:
                        storeCollector();
                        stopwatch.reset();
                        drive.turnTo(drive.imu.headingOffset - 155);
                        if (drive.atTargetHeading() || stopwatch.get() > 3.5) {
                            sequence.nextPhase();
                            stopwatch.reset();
                        }
                        break;

                    case 3:
                        collectorLift.setPower(0);
                        drive.moveAtAngle(0.55, 90);
                        stopwatch.unpause();
                        if (stopwatch.get() > 2) {
                            stopwatch.reset();
                            sequence.nextPhase();
                        }
                        break;

                    case 4:
                        drive.turnTo(-90);
                        stopwatch.unpause();
                        if (drive.atTargetHeading() || stopwatch.get() > 2) {
                            drive.stop();
                            sequence.nextPhase();
                            stopwatch.reset();
                            drive.startEncoders();
                        }
                        break;

                    case 5:
                        stopwatch.unpause();
                        drive.moveAtAngle(0.5, 0);
                        if(drive.atEncoderValue(1000) && depotIn == 0)
                            depotIn = totalStopwatch.get();
                        if (drive.atEncoderValue(1500) || stopwatch.get() > 4) {
                            drive.stop();
                            stopwatch.reset();
                            sequence.nextPhase();
                        }
                        break;

                    case 6:
                        if(collectorLift.getCurrentPosition() > 1000) {
                            stopwatch.unpause();
                            collector.setPower(0.5);
                        }
                        if(stopwatch.get() > 0.3) {
                            drive.moveAtAngle(Range.scale(stopwatch.get(), 0.3, 3, 0.7, 0.2), 180);
                        }
                        if(stopwatch.get() > 1.2) {
                            raiseCollector();
                            if(depotOut == 0)
                                depotOut = totalStopwatch.get();
                        } else
                            lowerCollector();
                        if (stopwatch.get() > 4.3) {
                            sequence.nextState();
                            stopwatch.reset();
                            drive.stop();
                            collector.setPower(0);
                            parked = totalStopwatch.get();
                        }
                        break;


                }
                break;

            case STOP:
                drive.stop();
                depositorExtension.setPower(0);
                telemetry.addData("goldOff", round(goldOff, 2));
                telemetry.addData("depotIn", round(depotIn, 2));
                telemetry.addData("depotOut", round(depotOut, 2));
                telemetry.addData("parked", round(parked, 2));


                break;
        }

        vision.endLoop();
        drive.imu.endLoop();

        telemetry.addData(sequence.telemetry(), "\n" + drive.imu.telemetry() + "\n" + drive.telemetry());

    }

    @Override
    public void stop() {

        drive.stop();

    }

    private void raiseDepositor() {
        depositorExtension.setPower(Range.clip((2300 - depositorExtension.getCurrentPosition()) / 75, 0.05, 1));
    }

    private void lowerDepositor() {
        depositorExtension.setPower(Range.clip(-depositorExtension.getCurrentPosition() / 75, -0.5, 0.3));
    }

    private void storeCollector() {
        collectorLift.setPower(Range.clip((collectorLift.getCurrentPosition() - 150) / 35, -0.25, 0.25));
    }

    private void raiseCollector() {
        collectorLift.setPower(Range.clip((collectorLift.getCurrentPosition() - 350) / 30, -0.35, 0.7));
    }

    private void lowerCollector(double speed) {
        collectorLift.setPower(Range.clip((collectorLift.getCurrentPosition() - 1050) / 70, -speed, 0.1));
    }

    private void lowerCollector() {
        lowerCollector(0.6);
    }

    private static double round (double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

}
