package org.firstinspires.ftc.teamcode.Teleops;

import com.google.gson.InstanceCreator;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

import static com.qualcomm.robotcore.hardware.DcMotor.ZeroPowerBehavior.BRAKE;

@Disabled
@TeleOp(name = "Calix Test", group = "Test")
public class CalixTest extends OpMode {

  Servo colRaise = null;

  static final double RAISER_UP_POS = 0.31;
  static final double RAISER_DOWN_POS = 1;
  static final double RAISER_INCREMENT = 0.0005;


  double raisePosition = 0.5;

  @Override
  public void init() {

    colRaise = hardwareMap.servo.get("colRaise");

  }

  @Override
  public void init_loop() {


  }

  @Override
  public void start() {



  }

  @Override
  public void loop() {

    if(gamepad1.dpad_up)
      raisePosition = RAISER_UP_POS;
    else if(gamepad1.left_bumper)
      raisePosition = RAISER_DOWN_POS;
    else if(gamepad1.dpad_left)
      raisePosition+= RAISER_INCREMENT;
    else if(gamepad1.dpad_right)
      raisePosition-= RAISER_INCREMENT;

    raisePosition = Range.clip(raisePosition, RAISER_UP_POS, RAISER_DOWN_POS);

    colRaise.setPosition(raisePosition);

  }

  @Override
  public void stop() {

    colRaise.setPosition(0.5);

  }

}
