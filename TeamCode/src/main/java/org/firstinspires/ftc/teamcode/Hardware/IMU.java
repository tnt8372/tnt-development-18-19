package org.firstinspires.ftc.teamcode.Hardware;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.util.Hardware;

import org.firstinspires.ftc.robotcore.external.navigation.Acceleration;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.Position;
import org.firstinspires.ftc.robotcore.external.navigation.Velocity;

public class IMU {

    private HardwareMap hwMap = null;

    private boolean isUpdated;

    private BNO055IMU IMU = null;

    private Orientation orientation;
    private double[] angles;

    public double headingOffset;

    public IMU() {
    }

    public void init(HardwareMap ahwMap) {

        hwMap = ahwMap;

        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.calibrationDataFile = "BNO055IMUCalibration.json"; // see the calibration sample opmode
        parameters.loggingEnabled = true;
        parameters.loggingTag = "IMU";
        parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();

        IMU = hwMap.get(BNO055IMU.class, "imu");
        IMU.initialize(parameters);

        IMU.startAccelerationIntegration(new Position(), new Velocity(), 1000);

        isUpdated = false;

        angles = new double[]{0, 0, 0};

    }

    public void endLoop() {
        isUpdated = false;
    }

    private void updateIMU() {
        isUpdated = true;
        orientation = IMU.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
        angles[0] = -normalizeAngle(orientation.firstAngle + headingOffset);
        angles[1] = normalizeAngle(orientation.secondAngle);
        angles[2] = normalizeAngle(orientation.thirdAngle);
    }

    public void setHeadingOffset(double offset) {
        headingOffset = offset;
    }

    public double[] getAngles() {
        if (!isUpdated) updateIMU();
        return angles;
    }

    public double getHeading() {
        if (!isUpdated) updateIMU();
        return angles[0];
    }

    public double normalizeAngle(double angle) { //turns angle to -180 to 180 deg
        while (angle < -180)
            angle += 360;
        while (angle > 180)
            angle -= 360;
        return angle;
    }

    public double denormalizeAngle(double angle) { //turns angle to 0 to 360 deg
        while (angle < 0)
            angle += 360;
        while (angle > 360)
            angle -= 360;
        return angle;
    }

    public String telemetry() {

        if (!isUpdated) updateIMU();

        String telem = String.format("IMU (x/y/z): %.0f/%.0f/%.0f", angles[0], angles[1], angles[2]);

        return telem;

    }

}
