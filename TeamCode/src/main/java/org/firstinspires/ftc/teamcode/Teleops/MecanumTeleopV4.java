package org.firstinspires.ftc.teamcode.Teleops;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.Hardware.IMU;
import org.firstinspires.ftc.teamcode.Hardware.Stopwatch;

import static org.firstinspires.ftc.teamcode.Hardware.ServoPositions.COLLECTOR_CLOSED;
import static org.firstinspires.ftc.teamcode.Hardware.ServoPositions.COLLECTOR_OPEN;
import static org.firstinspires.ftc.teamcode.Hardware.ServoPositions.DEPOSIT_CLOSED;
import static org.firstinspires.ftc.teamcode.Hardware.ServoPositions.DEPOSIT_DOWN;
import static org.firstinspires.ftc.teamcode.Hardware.ServoPositions.DEPOSIT_FLAT;
import static org.firstinspires.ftc.teamcode.Hardware.ServoPositions.DEPOSIT_OPEN;
import static org.firstinspires.ftc.teamcode.Hardware.ServoPositions.DEPOSIT_UP;


@TeleOp(name = "Better Mecanum Drive", group = "Teleop")
public class MecanumTeleopV4 extends OpMode {

    DcMotor motorTR = null;
    DcMotor motorTL = null;
    DcMotor motorBR = null;
    DcMotor motorBL = null;

    DcMotor collectorExtension = null;
    DcMotor depositorExtension = null;
    DcMotor lift = null;
    DcMotor collectorLift = null;

    Servo depositorGate = null;
    Servo collectorGate = null;
    CRServo collector = null;
    Servo depositorAngle = null;

    DigitalChannel liftLimit = null;
    DigitalChannel collectorLimit = null;
    DigitalChannel depositorLimit = null;

    IMU imu = null;

    double holdAngle = 0;

    double turningPower;

    int collectorPosition = 0;
    int depositorPosition = 0; //-1 is bottom-state, 0 is middling, 1 is top-state
    int flipPosition = 0;
    int gatePosition = 0;

    int collectorEncoder = 0;
    int depositorEncoder = 0;
    int flipEncoder = 0;

    int[] depositorHistory;
    int depositorHistoryIndex = 0;
    int lastDepotEncoder = 0;
    int totalDepositorHistory = 0;

    int autoPhase = 0;
    boolean onlyTransition = false;

    boolean lowering = false;

    double prop = 0;

    Stopwatch stopwatch = null;

    @Override
    public void init() {

        motorTR = hardwareMap.dcMotor.get("motorTR");
        motorTL = hardwareMap.dcMotor.get("motorTL");
        motorBR = hardwareMap.dcMotor.get("motorBR");
        motorBL = hardwareMap.dcMotor.get("motorBL");

        collectorExtension = hardwareMap.dcMotor.get("collectorExtension");
        depositorExtension = hardwareMap.dcMotor.get("depositorExtension");
        lift = hardwareMap.dcMotor.get("lift");
        collectorLift = hardwareMap.dcMotor.get("collectorLift");

        depositorGate = hardwareMap.servo.get("depositorGate");
        collector = hardwareMap.crservo.get("collector");
        collectorGate = hardwareMap.servo.get("collectorGate");
        depositorAngle = hardwareMap.servo.get("depositorAngle");

        liftLimit = hardwareMap.get(DigitalChannel.class, "liftLimit");
        collectorLimit = hardwareMap.get(DigitalChannel.class, "collectorLimit");
        depositorLimit = hardwareMap.get(DigitalChannel.class, "depositorLimit");

        collectorExtension.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        collectorLift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        collectorExtension.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        depositorExtension.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
        lift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorTR.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorTL.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorBR.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorBL.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        stopwatch = new Stopwatch();

        imu = new IMU();
        imu.init(hardwareMap);

        depositorHistory = new int[] {0, 0, 0, 0, 0};

    }

    @Override
    public void init_loop() {

    }

    @Override
    public void start() {
        collectorPosition = 0;
        depositorPosition = 0;
        stopwatch.reset();
    }

    @Override
    public void loop() {

        if (gamepad1.right_bumper)                               //bumper speeds
            prop = 0.5;
        else if (gamepad1.left_bumper)
            prop = 0.25;
        else
            prop = 1;

        if(gamepad1.x) {
            holdAngle = imu.getHeading();
        }


        if (gamepad1.right_trigger != 0) //lowering the ARM                      //lift
            lift.setPower(-gamepad1.right_trigger);
        else if (gamepad1.left_trigger != 0 && liftLimit.getState()) //raising the ARM
            lift.setPower(gamepad1.left_trigger);
        else
            lift.setPower(0);

        if (gamepad2.start)
            autoPhase = -1;
        else if (gamepad2.b || (!gamepad2.start && autoPhase == -1)) {
            lowering = true;
            depositorAngle.setPosition(DEPOSIT_UP);
            autoPhase = 0;
            flipPosition = 1;
            onlyTransition = false;
        }
        else if (gamepad2.x)
            autoPhase = 1;
        else if (gamepad2.y) {
            stopwatch.reset();
            autoPhase = 2;
        }
        else if (gamepad2.a) {
            autoPhase = 1;
            onlyTransition = true;
        }
        totalDepositorHistory -= depositorHistory[depositorHistoryIndex];
        depositorHistory[depositorHistoryIndex] = Math.abs(lastDepotEncoder - depositorExtension.getCurrentPosition());
        depositorHistoryIndex++;
        if(depositorHistoryIndex == 5) depositorHistoryIndex = 0;
        totalDepositorHistory += depositorHistory[depositorHistoryIndex];

        lastDepotEncoder = depositorExtension.getCurrentPosition();

        if(depositorExtension.getPower() == 0 && totalDepositorHistory < 5 && depositorHistoryIndex == 4)
            resetDepositorEncoder();

        telemetry.addData("depositorTotal", totalDepositorHistory);

        switch (autoPhase) {

            case -1: //resetting encoder values
                if (gamepad2.right_stick_y != 0 & gamepad1.start)
                    resetCollectorEncoder();
                if (gamepad2.left_stick_y != 0 && gamepad2.start)
                    resetDepositorEncoder();
                if (gamepad2.start && (gamepad2.dpad_down || gamepad2.dpad_right || gamepad2.dpad_left || gamepad2.dpad_up))
                    resetFlipEncoder();
                break;

            case 0:  //manual controls

                if(gamepad2.left_stick_y != 0)
                    lowering = false;

                /*   DEPOSITOR EXTENSION   */
                if(lowering) {
                    lowerDepositor();
                    if(getDepositorEncoder() < 1750)
                        depositorAngle.setPosition(DEPOSIT_FLAT);
                    else
                        depositorAngle.setPosition(DEPOSIT_UP);
                    if(getDepositorEncoder() < 300)
                        lowering = false;
                }
                else
                    depositorExtension.setPower(Range.clip(-gamepad2.left_stick_y, -1, 1));



                /*   COLLECTOR EXTENSION   */
                if(!collectorLimit.getState() && gamepad2.right_stick_y > 0)
                    collectorExtension.setPower(0);
                else
                    collectorExtension.setPower(-gamepad2.right_stick_y);



                /*   COLLECTOR FLIP STATE CONTROL   */
                if (gamepad2.dpad_left || gamepad2.dpad_right)
                    flipPosition = 0;
                else if (gamepad2.dpad_up)
                    flipPosition = 1;
                else if (gamepad2.dpad_down)
                    flipPosition = -1;

                /*   collector flipping   */
                if (flipPosition == 0) {
                    if (gamepad2.dpad_right)
                        collectorLift.setPower(0.55);
                    else if (gamepad2.dpad_left)
                        collectorLift.setPower(-0.75);
                    else
                        collectorLift.setPower(0);
                } else if (flipPosition == 1)
                    raiseCollector();
                else
                    lowerCollector();


                /*   COLLECTOR GATE   */
                if (gamepad2.left_bumper) {
                    collectorGate.setPosition(COLLECTOR_OPEN);
                } else
                    collectorGate.setPosition(COLLECTOR_CLOSED);



                /*   DEPOSITOR SERVO   */
                if (gamepad2.right_bumper)
                    depositorGate.setPosition(DEPOSIT_OPEN);
                else
                    depositorGate.setPosition(DEPOSIT_CLOSED);



                /*   COLLECTOR SERVO   */
                if (gamepad2.right_trigger != 0)
                    collector.setPower(-gamepad2.right_trigger * 0.85);
                else if (gamepad2.left_trigger != 0)
                    collector.setPower(gamepad2.left_trigger * 0.85);
                else
                    collector.setPower(0);



                /*   DEPOSITOR ANGLE   */
                if (!lowering && getDepositorEncoder() < 1750)
                    depositorAngle.setPosition(DEPOSIT_FLAT);
                else if(!lowering)
                    depositorAngle.setPosition(DEPOSIT_DOWN);

                break;


            case 1: //pulling back collector + transition

                collector.setPower(-0.85);

                depositorAngle.setPosition(DEPOSIT_FLAT);

                if (collectorLimit.getState() /*getCollectorEncoder() > 150*/) //if limit isn't pressed
                    collectorExtension.setPower(Range.clip(-getCollectorEncoder() / 75, -1, 0));
                else
                    collectorExtension.setPower(-0.2);

                lowerDepositor();

                if (getCollectorEncoder() < 900)
                    raiseCollector();

                if (!collectorLimit.getState() && getDepositorEncoder() < 100 && getFlipEncoder() > 300 && getFlipEncoder() < 400) {
                    collectorGate.setPosition(COLLECTOR_OPEN);
                    stopwatch.unpause();
                } else {
                    collectorGate.setPosition(COLLECTOR_CLOSED);
                    stopwatch.reset();
                }

                if(stopwatch.get() > 0.8) {
                    if(onlyTransition) {
                        autoPhase = 0;
                        onlyTransition = false;
                        flipPosition = 1;
                    } else
                        autoPhase = 2;
                    collector.setPower(0);
                    collectorGate.setPosition(COLLECTOR_CLOSED);
                    stopwatch.reset();
                    collectorExtension.setPower(0);
                }

                break;


            case 2: //raising depositor and depositing

                if(getDepositorEncoder() > 1750)
                    storeCollector();
                else
                    raiseCollector();

                if(stopwatch.get() < 0.8) {
                    raiseDepositor();
//                    if (getDepositorEncoder() > 1800)
//                        depositorAngle.setPosition(DEPOSIT_DOWN);
                    if(getDepositorEncoder() > 200)
                        depositorAngle.setPosition(DEPOSIT_DOWN);
                }
                else {
                    flipPosition = 1;
                    lowering = true;
                    autoPhase = 0;
                }

                if(getDepositorEncoder() > 2000)
                    depositorGate.setPosition(DEPOSIT_OPEN);

                if(getDepositorEncoder() > 2200)
                    stopwatch.unpause();

                break;



        }

        if(gamepad1.a){
            double speed = Range.clip(imu.normalizeAngle(holdAngle - imu.denormalizeAngle(imu.getHeading())) / 90, -1, 1);

            if(Math.abs(imu.denormalizeAngle(holdAngle) - imu.denormalizeAngle(imu.getHeading())) > 1.8)
                speed = Range.clip(Math.abs(speed), 0.18, 1) * Math.signum(speed);
            else
                speed = 0;
            turningPower = speed;
        }
        else {
            turningPower = gamepad1.right_stick_x;
        }
        motorTR.setPower(prop * Range.clip(+gamepad1.left_stick_x + gamepad1.left_stick_y + turningPower, -1, 1)); //motors
        motorTL.setPower(prop * Range.clip(+gamepad1.left_stick_x - gamepad1.left_stick_y + turningPower, -1, 1));
        motorBR.setPower(prop * Range.clip(-gamepad1.left_stick_x + gamepad1.left_stick_y + turningPower, -1, 1));
        motorBL.setPower(prop * Range.clip(-gamepad1.left_stick_x - gamepad1.left_stick_y + turningPower, -1, 1));

        telemetry.addData("depositor encoder", getDepositorEncoder());
        telemetry.addData("collector encoder", getCollectorEncoder());
        telemetry.addData("collector encoder 2", getFlipEncoder());

        telemetry.addData("heading", imu.getHeading());
        telemetry.addData("holdAngle", holdAngle);
        telemetry.addData("turningPower", turningPower);

        imu.endLoop();


    }

    @Override
    public void stop() {


    }

    private void resetDepositorEncoder() {
        depositorEncoder = depositorExtension.getCurrentPosition();
    }

    private void resetCollectorEncoder() {
        collectorEncoder = collectorExtension.getCurrentPosition();
    }

    private void resetFlipEncoder() {
        flipEncoder = collectorLift.getCurrentPosition() - 1130;
    }


    private int getDepositorEncoder() {
        return depositorExtension.getCurrentPosition() - depositorEncoder;


    }

    private int getCollectorEncoder() {
        return collectorExtension.getCurrentPosition() - collectorEncoder;
    }

    private int getFlipEncoder() {
        return collectorLift.getCurrentPosition() - flipEncoder;
    }


    private void raiseDepositor() {
        depositorExtension.setPower(Range.clip((2300 - getDepositorEncoder()) / 75, 0.05, 1));
    }

    private void lowerDepositor() {
        depositorExtension.setPower(Range.clip(-getDepositorEncoder() / 75, -0.25, 0.25));
    }

    private void storeCollector() {
        collectorLift.setPower(Range.clip((getFlipEncoder() - 150) / 35, -0.25, 0.25));
    }

    private void raiseCollector() {
        double speed = (getFlipEncoder() - 330.0) / 600.0;
        if (Math.abs(getFlipEncoder() - 350) > 23)
            speed = Range.clip(Math.abs(speed), 0.2, 1) * Math.signum(speed);
        else
            speed = 0;

        collectorLift.setPower(speed);
        telemetry.addData("collectorLift", speed);
    }

    private void lowerCollector() {
        collectorLift.setPower(Range.clip((getFlipEncoder() - 1130) / 75, -0.6, 0.6));
    }

    public void turnTo(double angle) {

        double speed = Range.clip(imu.normalizeAngle(angle - imu.denormalizeAngle(imu.getHeading())) / 110, -1, 1);

        if(Math.abs(imu.denormalizeAngle(angle) - imu.denormalizeAngle(imu.getHeading())) > 1.8)
            speed = Range.clip(Math.abs(speed), 0.2, 1) * Math.signum(speed);
        else
            speed = 0;


        motorTR.setPower(speed); //motors
        motorTL.setPower(speed);
        motorBR.setPower(speed);
        motorBL.setPower(speed);
    }

}
