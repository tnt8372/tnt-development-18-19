package org.firstinspires.ftc.teamcode.Teleops.RelicBot;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.GyroSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.TouchSensor;

/**
 * Created by Gordon on 1/29/2018.
 */

public abstract class DentKautermanBot extends OpMode{
  //hardware
  public DcMotor motorMR;
  public DcMotor motorML;
  public DcMotor motorBR;
  public DcMotor motorBL;

  public DcMotor motorRelic;
  public Servo relicGrabber;
  public CRServo relicLifter;

  public DcMotor motorGlyph;
  public DcMotor rightIntake;
  public DcMotor leftIntake;
  public Servo glyphLifter;

  public Servo jewelDeploy;
  public Servo jewelSweep;

  public GyroSensor gyro;

  public void init(){
    motorBL = hardwareMap.dcMotor.get("motorBL");
    motorML = hardwareMap.dcMotor.get("motorML");
    motorBR = hardwareMap.dcMotor.get("motorBR");
    motorMR = hardwareMap.dcMotor.get("motorMR");

    motorBL.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
    motorML.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
    motorBR.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
    motorMR.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);

    motorRelic = hardwareMap.dcMotor.get("motorRelic");

    motorGlyph = hardwareMap.dcMotor.get("motorGlyph");

    rightIntake = hardwareMap.dcMotor.get("rightIntake");
    leftIntake = hardwareMap.dcMotor.get("leftIntake");
        rightIntake.setDirection(DcMotorSimple.Direction.REVERSE);


    rightIntake.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    leftIntake.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

    glyphLifter = hardwareMap.servo.get("glyphLifter");

    jewelDeploy = hardwareMap.servo.get("servoJewelDeploy");
    jewelSweep = hardwareMap.servo.get("servoJewelSweep");

    relicGrabber = hardwareMap.servo.get("servoRelic");
    relicLifter = hardwareMap.crservo.get("servoRelicLift");

    gyro = hardwareMap.gyroSensor.get("gyro");
  }
}