package org.firstinspires.ftc.teamcode.Tests;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;

import org.firstinspires.ftc.teamcode.Hardware.VisionTracker;

//@Disabled
@Autonomous(name = "Tensor Test", group = "Test")
public class TensorTest extends OpMode {

  VisionTracker vision;

  @Override
  public void init() {

    vision = new VisionTracker();
    vision.init(hardwareMap);

  }

  @Override
  public void init_loop() {



  }

  @Override
  public void start() {

    vision.activateTensor();

  }

  @Override
  public void loop() {

    telemetry.addData("Gold is at x/y", vision.goldCoordBoth()[0] + "/" + vision.goldCoordBoth()[1]);

  }

  @Override
  public void stop() {

    vision.shutdownTensor();

  }

}
