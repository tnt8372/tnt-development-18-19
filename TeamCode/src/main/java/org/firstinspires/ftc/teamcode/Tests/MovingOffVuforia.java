package org.firstinspires.ftc.teamcode.Tests;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;

import org.firstinspires.ftc.teamcode.Hardware.MecanumDriveTrain;
import org.firstinspires.ftc.teamcode.Hardware.VisionTracker;

@Disabled
@Autonomous(name = "Vuforia Movement Test", group = "Test")
public class MovingOffVuforia extends OpMode {

  MecanumDriveTrain drive;
  VisionTracker vuforia;

  @Override
  public void init() {

    drive = new MecanumDriveTrain();
    drive.init(hardwareMap);

    vuforia = new VisionTracker();
    vuforia.init(hardwareMap);

  }

  @Override
  public void init_loop() {



  }

  @Override
  public void start() {

    vuforia.activateVuforia();

  }

  @Override
  public void loop() {

    double[] currCoords = {vuforia.getTranslation()[0], vuforia.getTranslation()[1]};
    double[] targCoords = {-130, 0};

    telemetry.addData("current x/y", vuforia.getTranslation()[0]+"/"+vuforia.getTranslation()[1]);
    telemetry.addData("translated x/y", (targCoords[0]-currCoords[0])+"/"+(targCoords[1]-currCoords[1]));

    drive.moveTowardsCoords(currCoords, targCoords);

    vuforia.endLoop();
    drive.imu.endLoop();

  }

  @Override
  public void stop() {

    vuforia.deactivateVuforia();

  }

}
