package org.firstinspires.ftc.teamcode.Tests;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.Hardware.MecanumDriveTrain;

@Disabled
@TeleOp(name = "MoveToCoords Test", group = "Test")
public class LiveMecanumHardwareTest extends OpMode {

    MecanumDriveTrain drive;

    DcMotor depositorExtension;

    double[] currCoords;
    double[] targCoords;

    @Override
    public void init() {

        drive = new MecanumDriveTrain();
        drive.init(hardwareMap);

        currCoords = new double[]{0, 0};
        targCoords = new double[]{0, 0};

        depositorExtension = hardwareMap.dcMotor.get("depositorExtension");

    }

    @Override
    public void init_loop() {


    }

    @Override
    public void start() {
        resetStartTime();
    }

    @Override
    public void loop() {

        depositorExtension.setPower(Range.clip((-depositorExtension.getCurrentPosition() - 950) / 75, -1, -0.05));

        telemetry.addData("encoders", depositorExtension.getCurrentPosition());
        telemetry.addData("speed", depositorExtension.getPower());
        telemetry.addData("math", (depositorExtension.getCurrentPosition() - 1100) / 30);


//    if(getRuntime() < 1)
//      drive.moveAtAngle(0.5, 0);
//    else if(getRuntime() < 2)
//      drive.moveAtAngle(0.5, 90);
//    else if(getRuntime() < 3)
//      drive.moveAtAngle(0.5, 180);
//    else if(getRuntime() < 4)
//      drive.moveAtAngle(0.5, -90);
//    else
//      drive.stop();


        //drive.moveAtAngle(1,90);
//    drive.setSpeeds(0, 0.1, 0, 1);

//    targCoords[0] = gamepad1.left_stick_x;
//    targCoords[1] = -gamepad1.left_stick_y;
//
//    drive.moveTowardsCoords(currCoords, targCoords);
//
//    drive.imu.endLoop();

    }

    @Override
    public void stop() {

    }

    private double[] generateCoords(double theta, double radius) {

        double[] coords = {0, 0};

        coords[0] = radius * Math.sin(Math.toRadians(theta));
        coords[1] = radius * Math.cos(Math.toRadians(theta));

        return coords;
    }

}
