package org.firstinspires.ftc.teamcode.Hardware;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.Telemetry;

import java.util.ArrayList;

/**
 * Created by Gordon on 10/21/2017.
 */

@Disabled
public class Sequence {

    private ArrayList<State> sequence;
    private int currIndex;
    private int phase;
    public int nextDelay = 1;

    public Sequence() {
        this.sequence = new ArrayList<State>();
        this.currIndex = 0;
        this.nextDelay = 1;
    }

    public Sequence(Sequence sequence2) {
        this.sequence = new ArrayList<State>(sequence2.getSequence());
        this.currIndex = 0;
        this.nextDelay = 1;
    }

    public ArrayList<State> getSequence() {
        return sequence;
    }

    public State current() {
        return sequence.get(currIndex);
    }

    public State nextState() {
        currIndex++;
        phase = 0;
        nextDelay = 1;
        return sequence.get(currIndex);
    }

    public int nextPhase() {
        return setPhase(++phase);
    }

    public int setPhase(int phase_){
        phase= Range.clip(phase_, 0, current().maxPhase());
        return phase;
    }

    public int getPhase() {
        return phase;
    }

    public State rewind() {
        currIndex = 0;
        return sequence.get(currIndex);
    }

    public boolean set(int index, State setState) {
        if (index < 0 || index >= sequence.size())
            return false;
        sequence.set(index, setState);
        return true;
    }

    public boolean setState(State setState) {
        sequence.set(this.currIndex, setState);
        return true;
    }

    private int findIndex(State state_) {
        for (int i = 0; i < State.VALUES.length; i++) {
            if (State.VALUES[i] == state_)
                return i;
        }
        return -1;
    }

    public void add(State setState) {
        sequence.add(setState);
    }


    public void clear() {
        sequence.removeAll(sequence);
    }

    public State nextValue(State state_) {
        int stateIndex = findIndex(state_);
        return State.VALUES[(stateIndex + 1) % (State.VALUES.length - 1)];
    }

    public State prevValue(State state_) {
        int stateIndex = findIndex(state_);
        if (stateIndex == 0)
            return State.VALUES[State.VALUES.length - 1];
        else
            return State.VALUES[(stateIndex - 1)];
    }

    public boolean remove(int index) {
        if (sequence.get(index) == null)
            return false;
        sequence.remove(index);
        if (sequence.size() == 0)
            sequence.add(State.VALUES[0]);
        return true;
    }

    public int size() {
        return sequence.size();
    }

    public boolean insertAfter(int index, State setState) {
        if (sequence.get(index) == null)
            return false;
        sequence.add(index + 1, setState);
        return true;
    }

    public boolean insertBefore(int index, State setState) {
        if (sequence.get(index) == null)
            return false;
        sequence.add(index, setState);
        return true;
    }

    public boolean swap(int a, int b) {
        if (sequence.get(a) == null || sequence.get(b) == null)
            return false;
        State temp = sequence.get(a);
        sequence.set(a, sequence.get(b));
        sequence.set(b, temp);
        return true;
    }

    public State get(int num) {
        return sequence.get(num);
    }
    public int getCurrIndex(){
        return currIndex;
    }

    public String telemetry() {
        String ans = "State: " + this.current().message();
        ans += "\nPhase: " + this.phase;
        return ans;
    }

    public void print(int select, Telemetry telemetry) {
        for (int i = 0; i < sequence.size(); i++) {
            String stateName = i + ". " + sequence.get(i).message();
            if (i == select)
                telemetry.addData("[" + stateName + "]", "");
            else
                telemetry.addData(" " + stateName, "");
        }
    }

}
