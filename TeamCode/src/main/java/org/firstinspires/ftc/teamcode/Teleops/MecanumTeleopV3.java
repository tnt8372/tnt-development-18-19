package org.firstinspires.ftc.teamcode.Teleops;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.Hardware.Stopwatch;


import static org.firstinspires.ftc.teamcode.Hardware.ServoPositions.COLLECTOR_CLOSED;
import static org.firstinspires.ftc.teamcode.Hardware.ServoPositions.COLLECTOR_OPEN;
import static org.firstinspires.ftc.teamcode.Hardware.ServoPositions.DEPOSIT_CLOSED;
import static org.firstinspires.ftc.teamcode.Hardware.ServoPositions.DEPOSIT_DOWN;
import static org.firstinspires.ftc.teamcode.Hardware.ServoPositions.DEPOSIT_FLAT;
import static org.firstinspires.ftc.teamcode.Hardware.ServoPositions.DEPOSIT_OPEN;
import static org.firstinspires.ftc.teamcode.Hardware.ServoPositions.DEPOSIT_UP;

@Disabled
@TeleOp(name = "Mecanum Drive", group = "Teleop")
public class MecanumTeleopV3 extends OpMode {

    DcMotor motorTR = null;
    DcMotor motorTL = null;
    DcMotor motorBR = null;
    DcMotor motorBL = null;

    DcMotor collectorExtension = null;
    DcMotor depositorExtension = null;
    DcMotor lift = null;
    DcMotor collectorLift = null;

    Servo depositorGate = null;
    Servo collectorGate = null;
    CRServo collector = null;
    Servo depositorAngle = null;

    DigitalChannel liftLimit = null;
    DigitalChannel collectorLimit = null;
    DigitalChannel depositorLimit = null;

    int collectorPosition = 0;
    int depositorPosition = 0; //-1 is bottom-state, 0 is middling, 1 is top-state
    int flipPosition = 0;
    int gatePosition = 0;

    int collectorEncoder = 0;
    int depositorEncoder = 0;
    int flipEncoder = 0;

    int a = 0, b = 0, c = 0, d = 0;

    double prop = 0;

    Stopwatch stopwatch = null;

    @Override
    public void init() {

        motorTR = hardwareMap.dcMotor.get("motorTR");
        motorTL = hardwareMap.dcMotor.get("motorTL");
        motorBR = hardwareMap.dcMotor.get("motorBR");
        motorBL = hardwareMap.dcMotor.get("motorBL");

        collectorExtension = hardwareMap.dcMotor.get("collectorExtension");
        depositorExtension = hardwareMap.dcMotor.get("depositorExtension");
        lift = hardwareMap.dcMotor.get("lift");
        collectorLift = hardwareMap.dcMotor.get("collectorLift");

        depositorGate = hardwareMap.servo.get("depositorGate");
        collector = hardwareMap.crservo.get("collector");
        collectorGate = hardwareMap.servo.get("collectorGate");
        depositorAngle = hardwareMap.servo.get("depositorAngle");

        liftLimit = hardwareMap.get(DigitalChannel.class, "liftLimit");
        collectorLimit = hardwareMap.get(DigitalChannel.class, "collectorLimit");
        depositorLimit = hardwareMap.get(DigitalChannel.class, "depositorLimit");

        collectorExtension.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        collectorLift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        collectorExtension.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        depositorExtension.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        lift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorTR.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorTL.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorBR.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorBL.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        stopwatch = new Stopwatch();

    }

    @Override
    public void init_loop() {

    }

    @Override
    public void start() {
        collectorPosition = 0;
        depositorPosition = 0;
        stopwatch.reset();
    }

    @Override
    public void loop() {

        if (gamepad1.right_bumper)                               //bumper speeds
            prop = 0.5;
        else if (gamepad1.left_bumper)
            prop = 0.25;
        else
            prop = 1;


        if (gamepad2.right_trigger != 0)                       //collector servo
            collector.setPower(-gamepad2.right_trigger * 0.85);
        else if (gamepad2.left_trigger != 0)
            collector.setPower(gamepad2.left_trigger * 0.85);
        else if (gamepad2.x && getCollectorEncoder() < 100 && getDepositorEncoder() < 100 && getFlipEncoder() > 300 && getFlipEncoder() < 400)
            collector.setPower(1);
        else
            collector.setPower(0);


        if (gamepad2.dpad_left || gamepad2.dpad_right)         //collector flipper
            flipPosition = 0;
        else if (gamepad2.dpad_up)
            flipPosition = 1;
        else if (gamepad2.dpad_down)
            flipPosition = -1;

        if (!gamepad2.start) {
            if (flipPosition == 0) {
                if (gamepad2.dpad_right)
                    collectorLift.setPower(0.55);
                else if (gamepad2.dpad_left)
                    collectorLift.setPower(-0.75);
                else
                    collectorLift.setPower(0);
            } else if (flipPosition == 1)
                collectorLift.setPower(Range.clip((getFlipEncoder() - 350) / 45, -0.25, 0.7));
            else
                collectorLift.setPower(Range.clip((getFlipEncoder() - 1100) / 75, -0.6, 0.6));
        } else if (gamepad2.dpad_left || gamepad2.dpad_right || gamepad2.dpad_up || gamepad2.dpad_down)
            resetFlipEncoder();


        if (gamepad2.left_bumper || (gamepad2.x && getCollectorEncoder() < 100 && getDepositorEncoder() < 100 && getFlipEncoder() > 300 && getFlipEncoder() < 400)) {                  //gates
            collectorGate.setPosition(COLLECTOR_OPEN);
        } else
            collectorGate.setPosition(COLLECTOR_CLOSED);

        if (gamepad2.right_bumper || gatePosition == 1)
            depositorGate.setPosition(DEPOSIT_OPEN);
        else
            depositorGate.setPosition(DEPOSIT_CLOSED);


        if (gamepad1.right_trigger != 0) //lowering the ARM                      //lift
            lift.setPower(-gamepad1.right_trigger);
        else if (gamepad1.left_trigger != 0 && liftLimit.getState()) //raising the ARM
            lift.setPower(gamepad1.left_trigger);
        else
            lift.setPower(0);


        if (gamepad2.y)                                        //depositor
            depositorPosition = 1;
        else if (gamepad2.left_stick_y != 0)
            depositorPosition = 0;

        if (!gamepad2.start) {
            if (depositorPosition == 0) {
                depositorExtension.setPower(Range.clip(-gamepad2.left_stick_y, -1, 1));
            } else if (depositorPosition == 1) {
                depositorExtension.setPower(Range.clip((3300 - getDepositorEncoder()) / 75, 0.05, 1));
                if (getDepositorEncoder() > 2800) {
                    gatePosition = 1;
                }
                if (getDepositorEncoder() > 3200) {
                    depositorPosition = 2;
                    stopwatch.unpause();
                }

            } else if (depositorPosition == -1) {
                depositorExtension.setPower(Range.clip(-getDepositorEncoder() / 75, -0.7, 0.3));
                if (getDepositorEncoder() < 1000)
                    gatePosition = 0;
            } else {
//        if(stopwatch.get() > 0.1) {
//          gatePosition = 1;
//        }
                if (stopwatch.get() > 0.8) {
                    stopwatch.reset();
                    depositorPosition = -1;
                }
            }
            if ((getDepositorEncoder() < 1750 && (depositorPosition == -1 || depositorPosition == 0)) || (getDepositorEncoder() < 750 && depositorPosition == 1) || gamepad2.x) {
                depositorAngle.setPosition(DEPOSIT_FLAT);
            } else if (depositorPosition == -1 && getDepositorEncoder() > 1750)
                depositorAngle.setPosition(DEPOSIT_UP);
            else
                depositorAngle.setPosition(DEPOSIT_DOWN);
        } else if (gamepad2.left_stick_y != 0)
            resetDepositorEncoder();


        if (gamepad2.x) {                                     //automatic transition
            depositorPosition = -1;
            collectorPosition = -1;
            if (getCollectorEncoder() < 2000)
                flipPosition = 1;
        } else
            collectorPosition = 0;


        if (!gamepad2.start) {
            if (collectorPosition == 0) {                                 //collector
                if (collectorLimit.getState() || gamepad2.right_stick_y < 0)
                    collectorExtension.setPower(-gamepad2.right_stick_y);
                else
                    collectorExtension.setPower(0);
            } else {
                if (!collectorLimit.getState())
                    collectorExtension.setPower(-0.2);
                else
                    collectorExtension.setPower(Range.clip(-getCollectorEncoder() / 75, -1, 0));
            }
        } else if (gamepad2.right_stick_y != 0)
            resetCollectorEncoder();


        motorTR.setPower(prop * Range.clip(+gamepad1.left_stick_x + gamepad1.left_stick_y + gamepad1.right_stick_x, -1, 1)); //motors
        motorTL.setPower(prop * Range.clip(+gamepad1.left_stick_x - gamepad1.left_stick_y + gamepad1.right_stick_x, -1, 1));
        motorBR.setPower(prop * Range.clip(-gamepad1.left_stick_x + gamepad1.left_stick_y + gamepad1.right_stick_x, -1, 1));
        motorBL.setPower(prop * Range.clip(-gamepad1.left_stick_x - gamepad1.left_stick_y + gamepad1.right_stick_x, -1, 1));

        telemetry.addData("depositor encoder", getDepositorEncoder());
        telemetry.addData("collector encoder", getCollectorEncoder());
        telemetry.addData("collector encoder 2", getFlipEncoder());
        telemetry.addData("a/b/c/d", a + "/" + b + "/" + c + "/" + d);


    }

    @Override
    public void stop() {


    }

    private void resetDepositorEncoder() {
        depositorEncoder = depositorExtension.getCurrentPosition();
    }

    private void resetCollectorEncoder() {
        collectorEncoder = collectorExtension.getCurrentPosition();
    }

    private void resetFlipEncoder() {
        flipEncoder = collectorLift.getCurrentPosition() - 1100;
    }


    private int getDepositorEncoder() {
        return depositorExtension.getCurrentPosition() - depositorEncoder;
    }

    private int getCollectorEncoder() {
        return collectorExtension.getCurrentPosition() - collectorEncoder;
    }

    private int getFlipEncoder() {
        return collectorLift.getCurrentPosition() - flipEncoder;
    }

}
