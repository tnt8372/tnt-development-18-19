package org.firstinspires.ftc.teamcode.Teleops.RelicBot;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.TouchSensor;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.Teleops.RelicBot.DentKautermanBot;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.lang.Math.abs;
import static java.lang.Math.signum;
import static java.lang.Math.sqrt;


/**
 * Created by Lance Parrish on 1/6/18.
 */

@Disabled
@TeleOp(name = "Old Teleop", group = "Old Bot")
public class OldBotTeleop extends DentKautermanBot {

  public final static double RELIC_START = 0;
  public final static double RELIC_OPEN = 0;
  public final static double RELIC_CLOSED = 1;

  public final static double RELIC_LIFT_UP = 0.8;
  public final static double RELIC_LIFT_DOWN = -0.7;

  public final static double JEWEL_DEPLOY_UP   = 0.33;
  public final static double JEWEL_DEPLOY_INIT = 0.09;
  public final static double JEWEL_DEPLOY_DOWN = 0.91;

  public final static double JEWEL_SWEEP_FWD  = 0.1;
  public final static double JEWEL_SWEEP_BACK = 1;
  public final static double JEWEL_SWEEP_MID  = 0.45;

  public final static double GLYPH_BRAKE = -0.1;

  public final static double GLYPH_LIFT_DOWN_OLD = 0.312;
  public final static double GLYPH_LIFT_DOWN = 0.291;
  public final static double GLYPH_LIFT_MID = 0.4;
  public final static double GLYPH_LIFT_UP = 0.9;

  double time = 0;

  double relicLiftPos = RELIC_LIFT_DOWN;

  final double deadZoneu = .07;
  final double deadZonev = .07;
  final double sin45 = -.7071; // sin(-45) to rotate ccw
  final double cos45 = .7071;  // cos(-45) to rotate ccw
  final double sqrt2 = 1.4142;

  boolean prevStart = false;
  boolean prevBack = false;
  boolean prevRightButton = false;
  boolean prevLeftButton = false;
  boolean prevLeftButton2 = false;

  boolean tankDrive = false;
  boolean coop = true;
  boolean slowExtension = false;
  boolean paused = true;

  //double leftWheelPower;
  //double rightWheelPower;

  public void start() {

    motorGlyph.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    motorRelic.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

    jewelDeploy.setPosition(JEWEL_DEPLOY_UP);
    jewelSweep.setPosition(JEWEL_SWEEP_FWD);
    relicGrabber.setPosition(RELIC_START);

    resetStartTime();
    time = 0;
  }

  public void loop() {
    jewelDeploy.setPosition(JEWEL_DEPLOY_UP);

    if(gamepad1.start&&!prevStart)
      paused = !paused;
    if(gamepad1.start&&gamepad1.right_bumper){
      time = 0;
      paused = true;

    }
    if(!paused) {
      time += getRuntime();
    }
    resetStartTime();
//        if(gamepad1.right_stick_button&&!prevRightButton)
//            tankDrive = !tankDrive;
//        if(gamepad1.left_stick_button&&!prevLeftButton)
//            rightHand = !rightHand;
    if (gamepad2.right_stick_button && !prevLeftButton2)
      slowExtension = !slowExtension;
    //slowExtension != slowExtension
    //nick was here

    double x;
    double y;

    if (tankDrive) {
      x = -gamepad1.left_stick_y;
      y = -gamepad1.right_stick_y;
    } else {

      double u = -0.8 * gamepad1.left_stick_x;//   stick x
      double v = gamepad1.left_stick_y;// - stick y //inert
      double rotu, rotv;
      // rotate 45 degrees

      u = (abs(u) > deadZoneu) ? u : 0;
      v = (abs(v) > deadZonev) ? v : 0;
      rotu = u * cos45 - v * sin45;
      rotv = v * cos45 + u * sin45;

      //No map
      x = rotu;
      y = rotv;
      double rotu2 = rotu * rotu;
      double rotv2 = rotv * rotv;
      //double rootsquare = sqrt(rotu2 + rotv2);
      //FG-squircular
      double bicubic = sqrt(rotu2 + rotv2 - (sqrt((rotu2 + rotv2) * (rotu2 + rotv2 - 4 * rotu2 * rotv2))));
      x = (rotv == 0) ? 0 : signum(rotu * rotv) / rotv / sqrt2 * bicubic;
      y = (rotu == 0) ? 0 : signum(rotu * rotv) / rotu / sqrt2 * bicubic;
    }

    double prop = gamepad1.a ? 0.5 : 1;

    if (gamepad1.left_bumper) {    // * .25
      x = .25 * x;
      y = .25 * y;
    } else if (gamepad1.right_bumper) {
      x *= 0.5;
      y *= 0.5;
    }

    motorML.setPower(Range.clip(x, -1.0, 1.0));
    motorBL.setPower(Range.clip(x, -1.0, 1.0));
    motorMR.setPower(Range.clip(-y, -1.0, 1.0));
    motorBR.setPower(Range.clip(-y, -1.0, 1.0));

//        if(gamepad1.left_stick_button){
//            motorBL.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
//            motorML.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
//            motorBR.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
//            motorMR.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
//        } else{
//            motorBL.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
//            motorML.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
//            motorBR.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
//            motorMR.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
//        }

    motorRelic.setPower(gamepad2.right_stick_x * (gamepad2.right_bumper ? 0.25 : 1));

    if (gamepad2.left_bumper && relicLifter.getPower() != RELIC_LIFT_UP) {
      relicGrabber.setPosition(RELIC_OPEN);
    } else
      relicGrabber.setPosition(RELIC_CLOSED);

    if (gamepad1.right_trigger != 0) {
      rightIntake.setPower(gamepad1.right_trigger * prop);
      leftIntake.setPower(gamepad1.right_trigger * prop);
    } else if (gamepad1.left_trigger != 0) {
      rightIntake.setPower(-gamepad1.left_trigger * prop);
      leftIntake.setPower(-gamepad1.left_trigger * prop);
    } else {
      rightIntake.setPower(0);
      leftIntake.setPower(0);
    }

    motorGlyph.setPower(0.75 * gamepad1.right_stick_y);

    if (gamepad1.x) {
      glyphLifter.setPosition(GLYPH_LIFT_UP);
    } else if (gamepad1.b) {
      glyphLifter.setPosition(GLYPH_LIFT_DOWN);
    } else if (gamepad1.y) {
      glyphLifter.setPosition(GLYPH_LIFT_MID);
    }

    if (gamepad2.dpad_up)
      relicLiftPos = RELIC_LIFT_UP;
    else if (gamepad2.dpad_down)
      relicLiftPos = RELIC_LIFT_DOWN;
    else if (gamepad2.dpad_left)
      relicLiftPos -= 0.005;
    else if (gamepad2.dpad_right)
      relicLiftPos += 0.005;

    relicLifter.setPower(relicLiftPos);



    prevBack = gamepad1.back;
    prevStart = (gamepad1.start) ? (gamepad1.start) : (gamepad2.start);
    prevRightButton = gamepad1.right_stick_button;
    prevLeftButton2 = gamepad2.right_stick_button;
//gordon got swag B)
    telemetry.addData("Time", (int)( time/60) + ":" + round(time%60, 2) + (paused ? "(paused)" : ""));
//        telemetry.addData("Active Players", (coop) ? ("Player 1 & 2") : ("Player 1"));
//        telemetry.addData("Drive Scheme", (tankDrive) ? ("Tank Drive") : ("1 Stick"));
//        telemetry.addData("Extension is", (slowExtension) ? ("slow") : ("fast"));
    telemetry.addData("glyphLift", motorGlyph.getPower());
    telemetry.update();

  }

  public double round(double value, int places) {
    if (places < 0) throw new IllegalArgumentException();

    BigDecimal bd = new BigDecimal(value);
    bd = bd.setScale(places, RoundingMode.HALF_UP);
    return bd.doubleValue();
  }
}
