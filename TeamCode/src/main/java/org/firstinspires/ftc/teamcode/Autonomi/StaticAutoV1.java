package org.firstinspires.ftc.teamcode.Autonomi;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;

import org.firstinspires.ftc.teamcode.Hardware.MecanumDriveTrain;
import org.firstinspires.ftc.teamcode.Hardware.Stopwatch;
import org.firstinspires.ftc.teamcode.Hardware.VisionTracker;

@Disabled
@Autonomous(name = "", group = "")
public class StaticAutoV1 extends OpMode {

    MecanumDriveTrain drive = null;

    Stopwatch stopwatch = null;

    VisionTracker vuforia = null;
    VisionTracker.rTrackables nearest = null;

    final static double[] RED_DEPOT = new double[]{60, -60};
    final static double[] BLUE_DEPOT = new double[]{-60, 60};

    int state = 0;

    @Override
    public void init() {

        stopwatch = new Stopwatch();

        drive = new MecanumDriveTrain();
        drive.init(hardwareMap);

        vuforia = new VisionTracker();
        vuforia.init(hardwareMap);

    }

    @Override
    public void init_loop() {


    }

    @Override
    public void start() {

        vuforia.activateVuforia();

    }

    @Override
    public void loop() {

        switch (state) {
            case 0:
                drive.turnTo(-45, 0.3);
                if (drive.atTargetHeading())
                    state++;
                break;

            case 1:
                drive.moveAtAngle(0.25, 65);
                if (vuforia.isTracking()) {
                    state++;
                    nearest = vuforia.nearestTrackable();
                }
                break;

            case 2:
                drive.moveTowardsCoordsTracking(vuforia.getTranslation(), RED_DEPOT, VisionTracker.rTrackables.CRATER.coords());
                if (vuforia.inArea(RED_DEPOT))
                    state++;
                break;

            case 3:
                stop();
                break;

        }

        telemetry.addData("state", state);

        telemetry.addData(drive.imu.telemetry(), "");
        telemetry.addData(drive.telemetry(), "");
        telemetry.addData(vuforia.telemetry(), "");

        drive.imu.endLoop();
        vuforia.endLoop();

    }

    @Override
    public void stop() {

        drive.stop();
        vuforia.deactivateVuforia();

    }

}
