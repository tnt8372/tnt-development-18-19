package org.firstinspires.ftc.teamcode.Teleops;

import com.qualcomm.hardware.rev.RevTouchSensor;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.util.Range;

import static android.os.FileObserver.OPEN;
import static org.firstinspires.ftc.teamcode.Hardware.ServoPositions.*;

@Disabled
@TeleOp(name = "Mecanum Drive old", group = "Teleop")
public class MecanumTeleopV2 extends OpMode {

  DcMotor motorTR = null;
  DcMotor motorTL = null;
  DcMotor motorBR = null;
  DcMotor motorBL = null;

  DcMotor collectorExtension = null;
  DcMotor depositorExtension = null;
  DcMotor lift = null;
  DcMotor collectorLift = null;

  Servo depositorGate = null;
  Servo collectorGate = null;
  CRServo collector = null;

  DigitalChannel liftLimit = null;
  DigitalChannel collectorLimit = null;
  DigitalChannel depositorLimit = null;

  double prop = 0;

  @Override
  public void init() {

    motorTR = hardwareMap.dcMotor.get("motorTR");
    motorTL = hardwareMap.dcMotor.get("motorTL");
    motorBR = hardwareMap.dcMotor.get("motorBR");
    motorBL = hardwareMap.dcMotor.get("motorBL");

    collectorExtension = hardwareMap.dcMotor.get("collectorExtension");
    depositorExtension = hardwareMap.dcMotor.get("depositorExtension");
    lift = hardwareMap.dcMotor.get("lift");
    collectorLift = hardwareMap.dcMotor.get("collectorLift");

    depositorGate = hardwareMap.servo.get("depositorGate");
    collector = hardwareMap.crservo.get("collector");
    collectorGate = hardwareMap.servo.get("collectorGate");

    liftLimit = hardwareMap.get(DigitalChannel.class, "liftLimit");
    collectorLimit = hardwareMap.get(DigitalChannel.class, "collectorLimit");
    depositorLimit = hardwareMap.get(DigitalChannel.class, "depositorLimit");

    collectorExtension.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

    collectorLift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    collectorExtension.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    depositorExtension.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    lift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    motorTR.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    motorTL.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    motorBR.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    motorBL.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);



  }

  @Override
  public void init_loop() {

  }

  @Override
  public void start() {

  }

  @Override
  public void loop() {

    if(gamepad1.right_bumper)
      prop = 0.5;
    else if(gamepad1.left_bumper)
      prop = 0.25;
    else
      prop = 1;

    if(gamepad2.right_trigger != 0)
      collector.setPower(-gamepad2.right_trigger*0.85);
    else if(gamepad2.left_trigger != 0)
      collector.setPower(gamepad2.left_trigger*0.85);
    else
      collector.setPower(0);

    if(gamepad2.dpad_up) {
      collectorLift.setPower(0.75);
    }
    else if(gamepad2.dpad_down) {
      collectorLift.setPower(-0.75);
    }
    else{
      collectorLift.setPower(0);
    }

    if(gamepad2.left_bumper)
      collectorGate.setPosition(COLLECTOR_OPEN);
    else
      collectorGate.setPosition(COLLECTOR_CLOSED);

    if(gamepad2.right_bumper)
      depositorGate.setPosition(DEPOSIT_OPEN);
    else
      depositorGate.setPosition(DEPOSIT_CLOSED) ;

    if(gamepad1.right_trigger != 0) //lowering the ARM
      lift.setPower(-gamepad1.right_trigger);
    else if(gamepad1.left_trigger != 0 && liftLimit.getState()) //raising the ARM
      lift.setPower(gamepad1.left_trigger);
    else
      lift.setPower(0);


    if(collectorLimit.getState() || gamepad2.right_stick_y < 0)
      collectorExtension.setPower(-gamepad2.right_stick_y);
    else
      collectorExtension.setPower(0.1);
    if(depositorLimit.getState() || gamepad2.left_stick_y < 0)
      depositorExtension.setPower(gamepad2.left_stick_y);
    else
      depositorExtension.setPower(0);
    motorTR.setPower(prop * Range.clip(+gamepad1.left_stick_x+gamepad1.left_stick_y+gamepad1.right_stick_x, -1, 1));
    motorTL.setPower(prop * Range.clip(+gamepad1.left_stick_x-gamepad1.left_stick_y+gamepad1.right_stick_x, -1, 1));
    motorBR.setPower(prop * Range.clip(-gamepad1.left_stick_x+gamepad1.left_stick_y+gamepad1.right_stick_x, -1, 1));
    motorBL.setPower(prop * Range.clip(-gamepad1.left_stick_x-gamepad1.left_stick_y+gamepad1.right_stick_x, -1, 1));

    telemetry.addData("depositor encoder", depositorExtension.getCurrentPosition());


  }

  @Override
  public void stop() {



  }

}
