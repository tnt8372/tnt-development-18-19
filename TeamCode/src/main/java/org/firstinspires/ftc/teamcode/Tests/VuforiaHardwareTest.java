package org.firstinspires.ftc.teamcode.Tests;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;

import org.firstinspires.ftc.teamcode.Hardware.VisionTracker;

@Disabled
@Autonomous(name = "Vuforia Test", group = "Test")
public class VuforiaHardwareTest extends OpMode {

  VisionTracker tracker;

  @Override
  public void init() {

    tracker = new VisionTracker();
    tracker.init(hardwareMap);

  }

  @Override
  public void init_loop() {



  }

  @Override
  public void start() {

    tracker.activateVuforia();

  }

  @Override
  public void loop() {

    telemetry.addData(tracker.telemetry(), "");

    tracker.endLoop();

  }

  @Override
  public void stop() {

    tracker.deactivateVuforia();

  }

}
