package org.firstinspires.ftc.teamcode.Hardware;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.util.Range;

public class MecanumDriveTrain {

    public DcMotor motorTR = null;
    public DcMotor motorTL = null;
    public DcMotor motorBR = null;
    public DcMotor motorBL = null;

    public IMU imu = null;

    private double targetHeading = 0;

    private int encoderBR = 0;
    private int encoderBL = 0;

    private int tempBR = 0;
    private int tempBL = 0;

    private int[] wheelMovement = null;
    private int wheelTotal = 0;
    private int wheelIndex = 0;

    private int accuracyCount = 0;

    private double speedIncrement = 0;

    private HardwareMap hwMap = null;

    public MecanumDriveTrain() {

        imu = new IMU();
        wheelMovement = new int[10];

        for (int i = 0; i < 10; i++)
            wheelMovement[i] = 0;

    }

    public void init(HardwareMap ahwMap) {

        hwMap = ahwMap;

        motorTR = hwMap.dcMotor.get("motorTR");
        motorTL = hwMap.dcMotor.get("motorTL");
        motorBR = hwMap.dcMotor.get("motorBR");
        motorBL = hwMap.dcMotor.get("motorBL");

        motorTR.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorTL.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorBR.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motorBL.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        imu.init(hwMap);

    }

    private double[] generateCoords(double theta, double radius) {

        double[] coords = {0, 0};
        theta = Math.toRadians(theta);

        coords[0] = radius * Math.sin(theta);
        coords[1] = radius * Math.cos(theta);

        return coords;
    }

    public void turnTo(double angle, double speed) {

        setTargetHeading(angle);

        if (stallTracker() <= 2)
            speedIncrement += 0.01;

        double sped = Range.clip(imu.normalizeAngle(angle - imu.denormalizeAngle(imu.getHeading())) / 110, -speed, speed);

        if(Math.abs(imu.denormalizeAngle(angle) - imu.denormalizeAngle(imu.getHeading())) > 1.8)
            sped = Range.clip(Math.abs(sped), 0.2, 1) * Math.signum(sped);
        else
            sped = 0;

        setSpeeds(sped, sped, sped, sped);

    }

    public void setOffset(int offset) {
        imu.setHeadingOffset(-offset);
        setTargetHeading(targetHeading + offset);
    }

    public void turnTo(double[] currCoords, double[] targCoords, double speed, double offset) {
        targCoords[0] -= currCoords[0];
        targCoords[1] -= currCoords[1];

        turnTo(Math.toDegrees(getAlpha(targCoords) - Math.toRadians(offset)), speed);
    }

    public void turnTo(double[] currCoords, double[] targCoords, double speed) {
        turnTo(currCoords, targCoords, speed, 0);
    }

    public void turnTo(double[] currCoords, double[] targCoords) {
        turnTo(currCoords, targCoords, 0.7, 0);
    }

    public void turnTo(double[] targCoords, double speed) {
        turnTo(getAlpha(targCoords), speed);
    }

    public void turnTo(double[] targCoords) {
        turnTo(getAlpha(targCoords));
    }

    public void turnTo(double angle) {
        turnTo(angle, 0.7);
    }

    public boolean atTargetHeading(double freedom) {

        double diff = Math.abs(imu.getHeading() - targetHeading);

        if(diff <= freedom)
            accuracyCount++;
        else
            accuracyCount = 0;

        return accuracyCount >= 5;

    }

    public boolean atTargetHeading() {
        return atTargetHeading(1.8);
    }

    public void moveAtAngle(double speed, double angle) {

        if (stallTracker() <= 2)
            speedIncrement += 0.01;

        speed += Math.signum(speed) * speedIncrement;

        double[] targCoords = generateCoords(angle, speed);

        double correction = (targetHeading - imu.getHeading()) / 50;

        motorTR.setPower(Range.clip(+targCoords[0] - targCoords[1] + correction, -speed, speed));
        motorTL.setPower(Range.clip(+targCoords[0] + targCoords[1] + correction, -speed, speed));
        motorBR.setPower(Range.clip(-targCoords[0] - targCoords[1] + correction, -speed, speed));
        motorBL.setPower(Range.clip(-targCoords[0] + targCoords[1] + correction, -speed, speed));
    }

    public void moveTowardsCoords(double[] currCoords, double[] targCoords, double speed) {

        targCoords[0] -= currCoords[0];
        targCoords[1] -= currCoords[1];

        double mag = Range.clip(Math.sqrt(targCoords[0] * targCoords[0] + targCoords[1] * targCoords[1]) / 20, -speed, speed);
        double alpha = Math.toDegrees(getAlpha(targCoords));

        alpha -= imu.getHeading();

        moveAtAngle(mag, alpha);
    }

    public void startEncoders() {
        encoderBL = motorBL.getCurrentPosition();
        encoderBR = motorBR.getCurrentPosition();
    }

    public boolean atEncoderValue(int value) {
        double avg = Math.abs(motorBL.getCurrentPosition() - encoderBL) / 2;
        avg += Math.abs(motorBR.getCurrentPosition() - encoderBR) / 2;
        return avg >= value;
    }

    private int stallTracker() {

        int currChange = Math.abs((motorBR.getCurrentPosition() + motorBL.getCurrentPosition() - tempBR - tempBL) / 2);

        wheelTotal -= wheelMovement[wheelIndex];
        wheelMovement[wheelIndex] = currChange;
        wheelTotal += wheelMovement[wheelIndex];

        tempBR = motorBR.getCurrentPosition();
        tempBL = motorBL.getCurrentPosition();

        wheelIndex++;
        if (wheelIndex == 10)
            wheelIndex = 0;

        return wheelTotal;

    }

    public void moveTowardsCoords(double[] currCoords, double[] targCoords) {
        moveTowardsCoords(currCoords, targCoords, 1);
    }

    public void moveTowardsCoordsTracking(double[] currCoords, double[] targCoords, double[] pointCoords, double speed) {

        pointCoords[0] -= currCoords[0];
        pointCoords[1] -= currCoords[1];

        setTargetHeading(Math.toDegrees(getAlpha(pointCoords)));

        moveTowardsCoords(currCoords, targCoords, speed);
    }

    public void moveTowardsCoordsTracking(double[] currCoords, double[] targCoords, double[] pointCoords) {
        moveTowardsCoordsTracking(currCoords, targCoords, pointCoords, 1);
    }

    public double getAlpha(double[] coords) {
        return Math.atan2(coords[0], coords[1]);
    }

    public void setTargetHeading(double heading) {
        targetHeading = imu.normalizeAngle(heading);
    }

    public void setSpeeds(double speedTR, double speedTL, double speedBR, double speedBL) {
        motorTR.setPower(speedTR);
        motorTL.setPower(speedTL);
        motorBR.setPower(speedBR);
        motorBL.setPower(speedBL);
    }

    public void stop() {
        setSpeeds(0, 0, 0, 0);
    }

    public String telemetry() {
        String telem = String.format("Drive (tr/tl/br/bl):\n  %.2f/%.2f/%.2f/%.2f", motorTR.getPower(), motorTL.getPower(), motorBR.getPower(), motorBL.getPower());
        telem += String.format("\n  target: %.0f", targetHeading);

        return telem;

    }

}
