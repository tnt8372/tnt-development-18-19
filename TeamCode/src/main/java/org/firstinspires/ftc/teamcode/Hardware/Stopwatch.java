package org.firstinspires.ftc.teamcode.Hardware;

import com.qualcomm.robotcore.util.ElapsedTime;

import java.util.ArrayList;
import java.util.List;

public class Stopwatch {

  private double startTime = 0;
  private double elapsedTime = 0;
  private boolean paused = true;

  private ElapsedTime runtime = new ElapsedTime();


  public Stopwatch() {
    runtime.reset();

  }

  public void reset() {
    elapsedTime = 0;
    paused = true;
  }

  public void unpause() {
    if(paused) {
      paused = false;
      startTime = runtime.milliseconds();
    }
  }

  public void pause() {
    if(!paused) {
      elapsedTime = this.get();
      paused = true;
    }
  }

  public double get() {
    return elapsedTime + ((paused) ? 0 : (runtime.milliseconds() - startTime)/1000);
  }


}
