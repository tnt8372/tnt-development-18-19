package org.firstinspires.ftc.teamcode.Hardware;

public enum State {

    DELAY("Delaying", 0),
    MOVE_FROM_LANDER("Moving from lander", 10),
    KNOCKING_GOLD_SAMPLE("Knocking off gold sample", 10),
    LATCH_PICTURE("Latch to picture", 10),
    MOVE_THRU_SAMPLE("Moving through sample to depot", 10),
    MOVE_TO_DEPOT("Moving to depot", 10),
    MOVE_TO_CRATER("Moving to crater", 10),
    STOP("Stopped", 0);


    private int maxPhase;

    private String message;

    public String message() {
        return message;
    }

    public int maxPhase() {
        return maxPhase;
    }

    private State(String message_, int maxPhase_) {
        this.message = message_;
        this.maxPhase = maxPhase_;
    }

    public final static State[] VALUES = State.values();

}

