package org.firstinspires.ftc.teamcode.Hardware;

public class ServoPositions {

    public final static double COLLECTOR_OPEN = 0;
    public final static double COLLECTOR_CLOSED = 0.75;

    public final static double DEPOSIT_OPEN = 0.95;
    public final static double DEPOSIT_CLOSED = 0.7;

    public final static double DEPOSIT_DOWN = 0.92;
    public final static double DEPOSIT_FLAT = 0.77;
    public final static double DEPOSIT_UP = 0.5;

}

