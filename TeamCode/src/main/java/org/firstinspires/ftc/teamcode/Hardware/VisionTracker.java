package org.firstinspires.ftc.teamcode.Hardware;

import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.util.Range;
import com.qualcomm.robotcore.util.RobotLog;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.matrices.MatrixF;
import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.matrices.VectorF;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

import java.util.ArrayList;
import java.util.List;

import static com.sun.tools.javac.util.Constants.format;
import static org.firstinspires.ftc.robotcore.external.navigation.AngleUnit.DEGREES;
import static org.firstinspires.ftc.robotcore.external.navigation.AxesOrder.XYZ;
import static org.firstinspires.ftc.robotcore.external.navigation.AxesOrder.YZX;
import static org.firstinspires.ftc.robotcore.external.navigation.AxesReference.EXTRINSIC;
import static org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit.mmPerInch;
import static org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection.BACK;
import static org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection.FRONT;

public class VisionTracker {

    public static final String TAG = "Vuforia Navigation Sample";

    private static final String VUFORIA_KEY = "AWh/9qH/////AAABmbYy//HKdEHdjk4YUDDIl950ifuQfjRu52OaJ9Wo+1inZ4ASno09ZjWqLmoPK092mmNLwbxsOWrt+KsWBUO99E+nDiK1s3vdN7QScBZeMJyyuLM2E+qLkT4jPp5w1PnuBGL6Up0u/Bi5kQQsKFpTkwcIFCmNj5ZIuwQlt/EwHChKmNAKE9gL2nZrTSoF8njozaz2ebSS+XXZJbM37IceDwzS/qShwEVlDnpz/GrAFWoSorz8E/vA3XD3BfflZH56BaSLbZ/PUiZyRpYbv/mDEgyyOVtb+AQcz2O7dZjlDlQhpjtO6j9XlYSvcQpN2WK+/VwITDHy8tSXpup+M/FcjzEimtgNMIOBIE0Vd/Iz/5Wb";
    private static final VuforiaLocalizer.CameraDirection CAMERA_CHOICE = FRONT;

    public boolean isUpdated;
    private boolean[] visiblePictures;
    private double[] orientation;
    private double[] translation;

    public int lastGoldPlacement = 1;

    private VuforiaLocalizer vuforia;
    VuforiaTrackables roverRuckus;

    VuforiaTrackable rover;
    VuforiaTrackable crater;
    VuforiaTrackable stars;
    VuforiaTrackable moon;

    List<VuforiaTrackable> allTrackables;

    private static final String TFOD_MODEL_ASSET = "RoverRuckus.tflite";
    private static final String LABEL_GOLD_MINERAL = "Gold Mineral";
    private static final String LABEL_SILVER_MINERAL = "Silver Mineral";

    private HardwareMap hwMap = null;

    private TFObjectDetector tfod;
    List<Recognition> updatedRecognitions = null;

    public enum rTrackables {
        ROVER(0, 0, 72),
        MOON(1, -72, 0),
        CRATER(2, 0, -72),
        STARS(3, 72, 0);

        private int index;
        private int x;
        private int y;

        rTrackables(int index, int x, int y) {
            this.index = index;
            this.x = x;
            this.y = y;
        }

        private int index() {
            return index;
        }

        public double[] coords() {
            double[] coords = new double[]{x, y};
            return coords;
        }

    }

    public VisionTracker() {
    }

    public void init(HardwareMap ahwMap) {

        hwMap = ahwMap;

        visiblePictures = new boolean[]{false, false, false, false};

        orientation = new double[]{0, 0, 0};
        translation = new double[]{0, 0, 0};


        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();

        parameters.vuforiaLicenseKey = VUFORIA_KEY;
        parameters.cameraDirection = CAMERA_CHOICE;

        vuforia = ClassFactory.getInstance().createVuforia(parameters);

        roverRuckus = this.vuforia.loadTrackablesFromAsset("RoverRuckus");

        allTrackables = new ArrayList<VuforiaTrackable>();

        rover = roverRuckus.get(0);
        rover.setName("Rover");  // Rover
        allTrackables.add(rover);

        moon = roverRuckus.get(1);
        moon.setName("Moon");  // Moon
        allTrackables.add(moon);

        crater = roverRuckus.get(2);
        crater.setName("Crater");  // Crater
        allTrackables.add(crater);

        stars = roverRuckus.get(3);
        stars.setName("Stars");  // Stars
        allTrackables.add(stars);

        float mmPerInch = 25.4f;
        float mmBotWidth = 18 * mmPerInch;
        float mmFTCFieldWidth = (12 * 12 - 2) * mmPerInch;
        final float mmTargetHeight = (6) * mmPerInch;


        OpenGLMatrix roverLocationOnField = OpenGLMatrix
                .translation(0, mmFTCFieldWidth / 2, mmTargetHeight)
                .multiplied(Orientation.getRotationMatrix(EXTRINSIC, XYZ, DEGREES, 90, 0, 0));
        rover.setLocation(roverLocationOnField);

        OpenGLMatrix moonLocationOnField = OpenGLMatrix
                .translation(0, -mmFTCFieldWidth / 2, mmTargetHeight)
                .multiplied(Orientation.getRotationMatrix(EXTRINSIC, XYZ, DEGREES, 90, 0, 180));
        moon.setLocation(moonLocationOnField);

        OpenGLMatrix craterLocationOnField = OpenGLMatrix
                .translation(-mmFTCFieldWidth / 2, 0, mmTargetHeight)
                .multiplied(Orientation.getRotationMatrix(EXTRINSIC, XYZ, DEGREES, 90, 0, 90));
        crater.setLocation(craterLocationOnField);

        OpenGLMatrix starsLocationOnField = OpenGLMatrix
                .translation(mmFTCFieldWidth / 2, 0, mmTargetHeight)
                .multiplied(Orientation.getRotationMatrix(EXTRINSIC, XYZ, DEGREES, 90, 0, -90));
        stars.setLocation(starsLocationOnField);

        final int CAMERA_FORWARD_DISPLACEMENT = 0;   // eg: Camera is 110 mm in front of robot center
        final int CAMERA_VERTICAL_DISPLACEMENT = 0;   // eg: Camera is 200 mm above ground
        final int CAMERA_LEFT_DISPLACEMENT = 0;     // eg: Camera is ON the robot's center line

        OpenGLMatrix phoneLocationOnRobot = OpenGLMatrix
                .translation(CAMERA_FORWARD_DISPLACEMENT, CAMERA_LEFT_DISPLACEMENT, CAMERA_VERTICAL_DISPLACEMENT)
                .multiplied(Orientation.getRotationMatrix(EXTRINSIC, YZX, DEGREES,
                        -90, 0, 0));

        for (VuforiaTrackable trackable : allTrackables) {
            ((VuforiaTrackableDefaultListener) trackable.getListener()).setPhoneInformation(phoneLocationOnRobot, parameters.cameraDirection);
        }

        int tfodMonitorViewId = hwMap.appContext.getResources().getIdentifier(
                "tfodMonitorViewId", "id", hwMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_GOLD_MINERAL, LABEL_SILVER_MINERAL);
    }

    public void activateVuforia() {
        roverRuckus.activate();
    }

    public void deactivateVuforia() {
        roverRuckus.deactivate();
    }

    public void activateTensor() {
        tfod.activate();
    }

    public void deactivateTensor() {
        tfod.deactivate();
    }

    public void shutdownTensor() {
        tfod.shutdown();
    }

    public boolean isVisible(rTrackables trackable) {
        if (!isUpdated) updateVuforia();
        return visiblePictures[trackable.index()];
    }

    public boolean isTracking() {
        if (!isUpdated) updateVuforia();
        return visiblePictures[0] || visiblePictures[1] || visiblePictures[2] || visiblePictures[3];
    }

    public rTrackables nearestTrackable(double[] currCoords) {
        double alpha = Math.toDegrees(Math.atan2(currCoords[0], currCoords[1]));

        if (alpha <= 45 && alpha > -45)
            return rTrackables.ROVER;
        else if (alpha <= 135 && alpha > 45)
            return rTrackables.STARS;
        else if (alpha <= -135 || alpha > 135)
            return rTrackables.MOON;
        else if (alpha <= -45 || alpha > -135)
            return rTrackables.CRATER;
        return rTrackables.CRATER;
    }

    public rTrackables nearestTrackable() {
        return nearestTrackable(getTranslation());
    }

    public void updateVuforia() {
        isUpdated = true;
        for (int i = 0; i < 4; i++) {
            OpenGLMatrix robotLocationTransform = ((VuforiaTrackableDefaultListener) allTrackables.get(i).getListener()).getUpdatedRobotLocation();
            visiblePictures[i] = ((VuforiaTrackableDefaultListener) allTrackables.get(i).getListener()).isVisible();
            if (robotLocationTransform != null) {
                orientation[0] = Orientation.getOrientation(robotLocationTransform, EXTRINSIC, XYZ, DEGREES).firstAngle;
                orientation[1] = Orientation.getOrientation(robotLocationTransform, EXTRINSIC, XYZ, DEGREES).secondAngle;
                orientation[2] = Orientation.getOrientation(robotLocationTransform, EXTRINSIC, XYZ, DEGREES).thirdAngle;
                translation[0] = robotLocationTransform.getTranslation().get(0) / 25.4;
                translation[1] = robotLocationTransform.getTranslation().get(1) / 25.4;
                translation[2] = robotLocationTransform.getTranslation().get(2) / 25.4;
            }
        }

    }

    public int[] goldCoordBoth() {
        int[] coords = {-1, 650};
        updatedRecognitions = tfod.getRecognitions();
        if (updatedRecognitions != null) {
            for (Recognition recognition : updatedRecognitions) {
                if (recognition.getLabel().equals(LABEL_GOLD_MINERAL) && recognition.getBottom() < coords[1]) {
                    coords[0] = (int) (recognition.getLeft() + recognition.getRight()) / 2;
                    coords[1] = (int) recognition.getBottom();
                }
            }
        }

        return coords;
    }

    public int goldCoord() { //0 - 1200 (0 is left, 1200 is right), returns -1 if nothing is seen
        return goldCoordBoth()[0];
    }

    public double goldAngle() {
        return Range.scale(goldCoord(), 0, 750, -19.5, 19.5);
    }

    public int searchGoldPlacement() { // -1 means gold is left, 0 means gold is middle, 1 means gold is right
        int gold = goldCoord();

        if (gold < 200 && gold >= 0)
            lastGoldPlacement = 0;
        else if (gold > 200)
            lastGoldPlacement = -1;

        return lastGoldPlacement;

    }

    public int angleOnGold(boolean startRight, boolean startRed) {
        if (!startRed) {
            if (!startRight) {
                switch (lastGoldPlacement) { //blue left
                    case -1:
                        return 64;
                    case 0:
                        return 90;
                    default:
                        return 117;
                }
            }
            switch (lastGoldPlacement) { //blue right
                case -1:
                    return 60;
                case 0:
                    return 90;
                default:
                    return 118;
            }
        }
        if (!startRight) {
            switch (lastGoldPlacement) { //red left
                case -1:
                    return 64;
                case 0:
                    return 90;
                default:
                    return 117;
            }
        }
        switch (lastGoldPlacement) { //red right
            case -1:
                return 59;
            case 0:
                return 90;
            default:
                return 120;
        }
    }

    public void endLoop() {
        for (int i = 0; i < 4; i++)
            visiblePictures[i] = false;
        isUpdated = false;
    }

    public double[] getOrientation() {
        if (!isUpdated) updateVuforia();
        return orientation;
    }

    public double[] getTranslation() {
        if (!isUpdated) updateVuforia();
        return translation;
    }

    public boolean inArea(double[] coords, double freedom) {

        double[] currCoords = getTranslation();

        if (currCoords[0] <= coords[0] + freedom && currCoords[0] >= coords[0] - freedom) {
            if (currCoords[1] <= coords[1] + freedom && currCoords[1] >= coords[1] - freedom)
                return true;
        }
        return false;

    }

    public boolean inArea(double[] coords) {
        return inArea(coords, 5);
    }

    public String telemetry() {
        if (!isUpdated) updateVuforia();
        String end = "Vuforia Telemetry:\n  ";
        end += "Seeing: " + (isTracking() ? "yes" : "no") + "\n  ";
        for (VuforiaTrackable trackable : allTrackables)
            end += trackable.getName() + ": " + (((VuforiaTrackableDefaultListener) trackable.getListener()).isVisible() ? "Visible" : "Not Visible") + "\n  ";

        if (isTracking()) {
            end += String.format("Pos (in): {X, Y, Z} = %.1f, %.1f, %.1f\n  ", getTranslation()[0], getTranslation()[1], getTranslation()[2]);
            end += String.format("Rot (deg): {Roll, Pitch, Heading} = %.0f, %.0f, %.0f", getOrientation()[0], getOrientation()[1], getOrientation()[2]);
        } else {
            end += "Pos (in): Unknown\n  Rot (deg): Unknown";
        }

        return end;

    }

}
